quickfix\_auto\_open.vim
========================

This is a tiny plugin packaging of hooks to automatically open relevant
quickfix and location lists when a command that changes their contents is run.

Credits
-------

Written and maintained by Tom Ryder <tom@sanctum.geek.nz>.  The original
commands are from Romain Lafourcade (romainl)'s "minivimrc" project on GitHub:
<https://github.com/romainl/minivimrc>

The only substantial changes to the original `autocmd` definitions  are special
handling for the unique behaviour of `:lhelpgrep:`.

License
-------

Distributed under the same terms as Vim itself, probably.  See `:help license`.

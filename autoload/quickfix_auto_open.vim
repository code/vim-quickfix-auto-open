" Handle a local quickfix command
function! quickfix_auto_open#Location(command) abort

  " The only difficult quickfix command to handle is :lhelpgrep, because it
  " uses the location list not for the current window but for a :help window,
  " creating a new one if necessary.  Worse, that window creation doesn't
  " happen until *after* this event has fired, so we have to defer the
  " location list opening with a dynamic autocommand that self-destructs.
  if a:command ==# 'lhelpgrep'

    " Expect a new window to pop up after this event has finished; hook into
    " it to check its location and the presence of a location list, and open
    " it up if so; then remove the hook
    autocmd quickfix_auto_open BufEnter *
          \ call s:Help(bufnr('%')) | autocmd! quickfix_auto_open BufEnter

  " All of the rest of the commands are really easy:
  else
    lwindow
  endif

endfunction

" If the buffer just entered is in a :help window with a location list with at
" least one item, pop it open
function! s:Help(bufnr) abort

  " Get buffer and window number
  let bufnr = a:bufnr
  let winnr = bufwinnr(bufnr)

  " Test buffer type and location list existence and non-zero contents
  if getbufvar(bufnr, '&buftype') ==# 'help'
        \ && len(getloclist(winnr)) > 0
    execute winnr.'windo lwindow'
  endif

endfunction

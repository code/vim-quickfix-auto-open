"
" quickfix_auto_open.vim: Always pop open the quickfix list or location list
" when they're changed.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_quickfix_auto_open') || &compatible || v:version < 700
  finish
endif
let loaded_quickfix_auto_open = 1

" Set up hooks for events that may yield a populated quickfix or location list
augroup quickfix_auto_open
  autocmd!
  autocmd VimEnter *
        \ cwindow
  autocmd QuickFixCmdPost [^l]*
        \ cwindow
  autocmd QuickFixCmdPost l*
        \ call quickfix_auto_open#Location(expand('<amatch>'))
augroup END
